<?php

class ebayAnalyticsApi{
    var $method;
    var $data;
    var $apiCallType;
    var $verb;
    var $additionalUrlParameters;
    var $authToken;

    public function __construct($analyticsApiEndpoint){
        $this->set_data(false);
        $this->accountApiEndpoint = $analyticsApiEndpoint;

    }

    function set_method($new_method){
        $this->method = $new_method;
    }

    function get_method(){
        return $this->method;
    }
    function set_data($new_data){
        $this->data = $new_data;
    }
    function get_data(){
        return $this->data;
    }
    function set_verb($new_verb){
        $this->verb = $new_verb;
    }
    function get_verb(){
        return $this->verb;
    }
    function set_additionalUrlParameters($new_additionalUrlParameters){
        $this->additionalUrlParameters = $new_additionalUrlParameters;
    }
    function get_additionalUrlParameters(){
        return $this->additionalUrlParameters;
    }

    function set_authToken($new_authToken){
        $this->authToken = $new_authToken;
    }
    function get_authToken(){
        return $this->authToken;
    }

    //====================================================================
    //  SELLER_STANDARDS_PROFILE CALLS
    //====================================================================

    function getASpecificSellerProfile($authToken, $program, $cycle){
        $this->clear();
        $this->set_method('GET');
        $this->set_data(false);
        $this->set_verb('seller_standards_profile/'.$program.'/'.$cycle);
        $this->set_additionalUrlParameters('');
        $this->set_authToken($authToken);
        $result = $this->CallEbayAnalyticsAPI();
        $resultArray = $this->buildReturnArray($result);
        return $resultArray;
    }
    function getAllSellerProfiles($authToken){
        $this->clear();
        $this->set_method('GET');
        $this->set_data(false);
        $this->set_verb('seller_standards_profile');
        $this->set_additionalUrlParameters('');
        $this->set_authToken($authToken);
        $result = $this->CallEbayAnalyticsAPI();
        $resultArray = $this->buildReturnArray($result);
        return $resultArray;
    }

    //====================================================================
    //  TRAFFIC_REPORT CALLS
    //====================================================================

    function getATrafficReport($authToken){
        $this->clear();
        $this->set_method('GET');
        $this->set_data(false);
        $this->set_verb('traffic_report');
        $this->set_additionalUrlParameters('');
        $this->set_authToken($authToken);
        $result = $this->CallEbayAnalyticsAPI();
        $resultArray = $this->buildReturnArray($result);
        return $resultArray;
    }
    //====================================================================
    //  SUPPORTING FUNCTIONS
    //====================================================================

    function urlBuilder(){
        $apiCallType = $this->get_verb();
        $additionalUrlParameters = $this->get_additionalUrlParameters();

        $baseEndpoint = $this->accountApiEndpoint.$apiCallType;
        if($additionalUrlParameters != ''){
            $baseEndpoint.="?".$additionalUrlParameters;
        }
        return $baseEndpoint;
    }

    function CallEbayAnalyticsAPI(){
        $method = $this->get_method();
        $dataToSendToEbay = $this->get_data();
        $authToken = $this->get_authToken();

        $curl = curl_init();
        switch ($method)
        {
            case "POST":
                curl_setopt($curl, CURLOPT_POST, 1);

                if ($dataToSendToEbay)
                    curl_setopt($curl, CURLOPT_POSTFIELDS, $dataToSendToEbay);
                break;
            case "PUT":
                curl_setopt($curl, CURLOPT_PUT, 1);
                break;
            case "GET";
                curl_setopt($curl, CURLOPT_HTTPGET, 1);

                break;
            default:
                if ($dataToSendToEbay)
                    $url = sprintf("%s?%s", $url, http_build_query($dataToSendToEbay));
        }

        curl_setopt($curl, CURLOPT_HTTPHEADER, array(
            'Authorization:Bearer '. $authToken,
            'Accept:application/json',
            'Content-Type: application/json',
            'Content-Language: en-US'
        ));
        $url = $this->urlBuilder();
        curl_setopt($curl, CURLOPT_URL, $url);
        curl_setopt($curl, CURLOPT_RETURNTRANSFER, 1);

        $result = curl_exec($curl);

        curl_close($curl);

        return $result;
    }

    function clear(){
        $this->set_method('GET');
        $this->set_data(false);
        $this->set_verb('');
        $this->set_additionalUrlParameters('');
        $this->set_authToken('');
    }

    function buildReturnArray($result){
        $verb = $this->get_verb();
        $method = $this->get_method();
        $data = $this->get_data();
        $additionalUrlParameters = $this->get_additionalUrlParameters();

        return array(
            'verb' => $verb,
            'method' => $method,
            'data' => $data,
            'additionalUrlParameters' => $additionalUrlParameters,
            'result' => $result
        );
    }

}//END class ebayAnalyticsApi