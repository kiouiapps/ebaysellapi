<?php
class ebayMarketingApi{
    var $method;
    var $data;
    var $apiCallType;
    var $verb;
    var $additionalUrlParameters;
    var $authToken;

    public function __construct($marketingApiEndpoint){
        $this->set_data(false);
        $this->accountApiEndpoint = $marketingApiEndpoint;

    }

    function set_method($new_method){
        $this->method = $new_method;
    }

    function get_method(){
        return $this->method;
    }
    function set_data($new_data){
        $this->data = $new_data;
    }
    function get_data(){
        return $this->data;
    }
    function set_verb($new_verb){
        $this->verb = $new_verb;
    }
    function get_verb(){
        return $this->verb;
    }
    function set_additionalUrlParameters($new_additionalUrlParameters){
        $this->additionalUrlParameters = $new_additionalUrlParameters;
    }
    function get_additionalUrlParameters(){
        return $this->additionalUrlParameters;
    }

    function set_authToken($new_authToken){
        $this->authToken = $new_authToken;
    }
    function get_authToken(){
        return $this->authToken;
    }

    //====================================================================
    //  AD CALLS
    //====================================================================

    //These requests require a user access token with the following scope https://api.ebay.com/oauth/api_scope/sell.marketing

//These two bulk create functions are structured the same but take different request payloads
    function bulkCreateAdsByInventoryReference($authToken, $data, $campaignId){
        $this->clear();
        $this->set_method('POST');
        $this->set_data($data);
        $this->set_verb('ad_campaign/'.$campaignId.'/bulk_create_ads_by_inventory_reference');
        $this->set_additionalUrlParameters('');
        $this->set_authToken($authToken);
        $result = $this->CallEbayMarketingAPI();
        $resultArray = $this->buildReturnArray($result);
        return $resultArray;
    }
    function bulkCreateAdsByListingId($authToken, $data, $campaignId){
        $this->clear();
        $this->set_method('POST');
        $this->set_data($data);
        $this->set_verb('ad_campaign/'.$campaignId.'/bulk_create_ads_by_listing_id');
        $this->set_additionalUrlParameters('');
        $this->set_authToken($authToken);
        $result = $this->CallEbayMarketingAPI();
        $resultArray = $this->buildReturnArray($result);
        return $resultArray;
    }


    
    /*
     * Bulk Delete Ads By Inventory Reference
     *          {
                    "requests": [ -OPTIONAL
                        {
                            "bidPercentage": string, -REQUIRED
                            "inventoryReferenceId": string, -REQUIRED
                            "inventoryReferenceType": string -REQUIRED
                                }
                           ]
                }
     */
    function bulkDeleteAdsByInventoryReference($authToken, $data, $campaignId){
        $this->clear();
        $this->set_method('POST');
        $this->set_data($data);
        $this->set_verb('ad_campaign/'.$campaignId.'/bulk_delete_ads_by_inventory_reference');
        $this->set_additionalUrlParameters('');
        $this->set_authToken($authToken);
        $result = $this->CallEbayMarketingAPI();
        $resultArray = $this->buildReturnArray($result);
        return $resultArray;

    }

    /*
     * Bulk Delete Ads By Listing Id
     *
     * {
        "requests": [  --OPTIONAL
            {
            "listingId": string --REQUIRED
              }
            More DeleteAdRequest nodes here
            ]
        }
     */
    function bulkDeleteAdsByListingId($authToken, $data, $campaignId){
        $this->clear();
        $this->set_method('POST');
        $this->set_data($data);
        $this->set_verb('ad_campaign/'.$campaignId.'/bulk_delete_ads_by_listing_id');
        $this->set_additionalUrlParameters('');
        $this->set_authToken($authToken);
        $result = $this->CallEbayMarketingAPI();
        $resultArray = $this->buildReturnArray($result);
        return $resultArray;
    }
    function bulkUpdateAdBidsByInventoryReference($authToken, $data, $campaignId){
        $this->clear();
        $this->set_method('POST');
        $this->set_data($data);
        $this->set_verb('ad_campaign'.$campaignId.'/bulk_update_ads_bid_by_inventory_reference');
        $this->set_authToken($authToken);
        $result = $this->CallEbayMarketingAPI();
        $resultArray = $this->buildReturnArray($result);
        return $resultArray;
    }
    function bulkUpdateAdBidsByListingId($authToken, $data, $campaignId){
        $this->clear();
        $this->set_method('POST');
        $this->set_data($data);
        $this->set_verb('ad_campaign'.$campaignId.'bulk_update_ads_bid_by_inventory_reference');
        $this->set_additionalUrlParameters('');
        $this->set_authToken($authToken);
        $result = $this->CallEbayMarketingAPI();
        $resultArray = $this->buildReturnArray($result);
        return $resultArray;
    }
    function createAdByListingId($authToken, $data, $campaignId){
        $this->clear();
        $this->set_method('POST');
        $this->set_data($data);
        $this->set_verb('ad_campaign/'.$campaignId.'/ad');
        $this->set_additionalUrlParameters('');
        $this->set_authToken($authToken);
        $result = $this->CallEbayMarketingAPI();
        $resultArray = $this->buildReturnArray($result);
        return $resultArray;
    }
    function createAdsByInventoryReference($authToken, $data, $campaignId){
        $this->clear();
        $this->set_method('POST');
        $this->set_data($data);
        $this->set_verb('ad_campaign/'.$campaignId.'/create_ads_by_inventory_reference');
        $this->set_additionalUrlParameters('');
        $this->set_authToken($authToken);
        $result = $this->CallEbayMarketingAPI();
        $resultArray = $this->buildReturnArray($result);
        return $resultArray;
    }
    function deleteAdById($authToken, $data, $campaignId, $adId){
        $this->clear();
        $this->set_method('DELETE');
        $this->set_data($data);
        $this->set_verb('ad_campaign/'.$campaignId.'/ad/'.$adId);
        $this->set_additionalUrlParameters('');
        $this->set_authToken($authToken);
        $result = $this->CallEbayMarketingAPI();
        $resultArray = $this->buildReturnArray($result);
        return $resultArray;
    }
    function deleteAdsByInventoryReference($authToken, $data, $campaignId){
        $this->clear();
        $this->set_method('POST');
        $this->set_data($data);
        $this->set_verb('ad_campaign/'.$campaignId.'/delete_ads_by_inventory_reference');
        $this->set_additionalUrlParameters('');
        $this->set_authToken($authToken);
        $result = $this->CallEbayMarketingAPI();
        $resultArray = $this->buildReturnArray($result);
        return $resultArray;
    }

    function getAdById($authToken, $campaignId, $adId){
        $this->clear();
        $this->set_method('GET');
        $this->set_data(false);
        $this->set_verb('ad_campaign/'.$campaignId.'/ad/'.$adId);
        $this->set_additionalUrlParameters('');
        $this->set_authToken($authToken);
        $result = $this->CallEbayMarketingAPI();
        $resultArray = $this->buildReturnArray($result);
        return $resultArray;
    }
    function getAds($authToken, $campaignId){
        $this->clear();
        $this->set_method('GET');
        $this->set_data(false);
        $this->set_verb('ad_campaign/'.$campaignId.'/ad');
        $this->set_additionalUrlParameters('');
        $this->set_authToken($authToken);
        $result = $this->CallEbayMarketingAPI();
        $resultArray = $this->buildReturnArray($result);
        return $resultArray;
    }
    function getAdsByInventoryReference($authToken, $campaignId){
        $this->clear();
        $this->set_method('GET');
        $this->set_data(false);
        $this->set_verb('ad_campaign/'.$campaignId.'get_ads_by_inventory_reference');
        $this->set_additionalUrlParameters('');
        $this->set_authToken($authToken);
        $result = $this->CallEbayMarketingAPI();
        $resultArray = $this->buildReturnArray($result);
        return $resultArray;
    }
    function updateBid($authToken, $data, $campaignId, $adId){
        $this->clear();
        $this->set_method('POST');
        $this->set_data($data);
        $this->set_verb('ad_campaign/'.$campaignId.'/ad/'.$adId.'/update_bid');
        $this->set_additionalUrlParameters('');
        $this->set_authToken($authToken);
        $result = $this->CallEbayMarketingAPI();
        $resultArray = $this->buildReturnArray($result);
        return $resultArray;
    }


    //====================================================================
    //  CAMPAIGN CALLS
    //====================================================================
    function cloneCampaign($authToken, $data, $campaignId){
        $this->clear();
        $this->set_method('POST');
        $this->set_data($data);
        $this->set_verb('ad_campaign/'.$campaignId.'/clone');
        $this->set_additionalUrlParameters('');
        $this->set_authToken($authToken);
        $result = $this->CallEbayMarketingAPI();
        $resultArray = $this->buildReturnArray($result);
        return $resultArray;
    }

    /* Create A Campaign
     * From http://developer.ebay.com/Devzone/rest/api-ref/marketing/ad_campaign__post.html
     * The inputs for this sample are campaignName, startDate, fundingStrategy.bidPercentage,
     * fundingStrategy.fundingModel and marketplaceId.
        Required Input
        {
       "campaignName":"Fall Sale",
       "startDate":"2016-09-07T21:43:00Z",
       "fundingStrategy":{
          "bidPercentage":"10.0",
          "fundingModel":"COST_PER_SALE"
       },
       "marketplaceId":"EBAY_US"
}
     */
    function createCampaign($authToken, $data){
        $this->clear();
        $this->set_method('POST');
        $this->set_data($data);
        $this->set_verb('ad_campaign');
        $this->set_additionalUrlParameters('');
        $this->set_authToken($authToken);
        $result = $this->CallEbayMarketingAPI();
        $resultArray = $this->buildReturnArray($result);
        return $resultArray;
    }
    function deleteCampaign($authToken, $data, $campaignId){
        $this->clear();
        $this->set_method('DELETE');
        $this->set_data($data);
        $this->set_verb('ad_campaign/'.$campaignId);
        $this->set_additionalUrlParameters('');
        $this->set_authToken($authToken);
        $result = $this->CallEbayMarketingAPI();
        $resultArray = $this->buildReturnArray($result);
        return $resultArray;
    }
    function endCampaign($authToken, $data, $campaignId){
        $this->clear();
        $this->set_method('POST');
        $this->set_data($data);
        $this->set_verb('ad_campaign/'.$campaignId.'/end');
        $this->set_additionalUrlParameters('');
        $this->set_authToken($authToken);
        $result = $this->CallEbayMarketingAPI();
        $resultArray = $this->buildReturnArray($result);
        return $resultArray;
    }
    function findCampaignByAdReference($authToken, $inventoryReferenceId=null, $inventoryReferenceType=null, $listingId=null){
        $this->clear();
        $this->set_method('GET');
        $this->set_data(false);
        $this->set_verb('ad_campaign/find_campaign_by_ad_reference');
        $this->set_additionalUrlParameters('inventory_reference_id='.urlencode($inventoryReferenceId).'&inventory_reference_type='.urlencode($inventoryReferenceType).'&listing_id='.urlencode($listingId));
        $this->set_authToken($authToken);
        $result = $this->CallEbayMarketingAPI();
        $resultArray = $this->buildReturnArray($result);
        return $resultArray;
    }
    function getAllCampaigns($authToken, $campaign_name=null, $campaignStatus=null, $endDateRange=null, $limit=null, $offset=null, $startDateRange=null){
        $this->clear();
        $this->set_method('GET');
        $this->set_data(false);
        $this->set_verb('ad_campaign');
        $this->set_additionalUrlParameters('campaign_name='.urlencode($campaign_name).'&campaign_status='.urlencode($campaignStatus).'&end_date_range='.urlencode($endDateRange).'&limit='.urlencode($limit).'&offset='.urlencode($offset).'&start_date_range='.urlencode($startDateRange));
        $this->set_authToken($authToken);
        $result = $this->CallEbayMarketingAPI();
        $resultArray = $this->buildReturnArray($result);
        return $resultArray;
    }
    function getCampaignByName($authToken, $campaignName){
        $this->clear();
        $this->set_method('GET');
        $this->set_data(false);
        $this->set_verb('ad_campaign/get_campaign_by_name');
        $this->set_additionalUrlParameters('campaign_name='.urlencode($campaignName));
        $this->set_authToken($authToken);
        $result = $this->CallEbayMarketingAPI();
        $resultArray = $this->buildReturnArray($result);
        return $resultArray;

    }
    function getCampaignById($authToken, $campaignId){
        $this->clear();
        $this->set_method('GET');
        $this->set_data(false);
        $this->set_verb('ad_campaign/'.urlencode($campaignId));
        $this->set_additionalUrlParameters('');
        $this->set_authToken($authToken);
        $result = $this->CallEbayMarketingAPI();
        $resultArray = $this->buildReturnArray($result);
        return $resultArray;
    }
    function pauseCampaign($authToken, $campaignId){
        $this->clear();
        $this->set_method('POST');
        $this->set_data(false);
        $this->set_verb('ad_campaign/'.urlencode($campaignId).'/pause');
        $this->set_additionalUrlParameters('');
        $this->set_authToken($authToken);
        $result = $this->CallEbayMarketingAPI();
        $resultArray = $this->buildReturnArray($result);
        return $resultArray;
    }
    function resumeCampaign($authToken, $campaignId){
        $this->clear();
        $this->set_method('POST');
        $this->set_data(false);
        $this->set_verb('ad_campaign/'.$campaignId.'/resume');
        $this->set_additionalUrlParameters('');
        $this->set_authToken($authToken);
        $result = $this->CallEbayMarketingAPI();
        $resultArray = $this->buildReturnArray($result);
        return $resultArray;
    }
    /* Update Campaign Identification
     * SAMPLE PAYLOAD - More info at http://developer.ebay.com/Devzone/rest/api-ref/marketing/ad_campaign-campaign_id_update_campaign_identification__post.html
     * {
        "campaignName": string, --REQUIRED
        "endDate": string,
        "startDate": string -REQUIRED
        }
     */
    function updateCampaignIdentification($authToken){
        $this->clear();
        $this->set_method('POST');
        $this->set_data(false);
        $this->set_verb('ad_campaign/'.$campaignId.'/update_campaign_identification');
        $this->set_additionalUrlParameters('');
        $this->set_authToken($authToken);
        $result = $this->CallEbayMarketingAPI();
        $resultArray = $this->buildReturnArray($result);
        return $resultArray;
    }
    //====================================================================
    //  ITEM_PROMOTION CALLS
    //====================================================================
    /*
     * Go here to see sample payloads: http://developer.ebay.com/Devzone/rest/api-ref/marketing/item_promotion__post.html. Most fields are conditional.
     */
    function createItemPromotion($authToken, $data){
        $this->clear();
        $this->set_method('POST');
        $this->set_data($data);
        $this->set_verb('item_promotion');
        $this->set_additionalUrlParameters('');
        $this->set_authToken($authToken);
        $result = $this->CallEbayMarketingAPI();
        $resultArray = $this->buildReturnArray($result);
        return $resultArray;
    }
    function deleteItemPromotion($authToken, $promotionId){
        $this->clear();
        $this->set_method('DELETE');
        $this->set_data(false);
        $this->set_verb('item_promotion/'.urlencode($promotionId));
        $this->set_additionalUrlParameters('');
        $this->set_authToken($authToken);
        $result = $this->CallEbayMarketingAPI();
        $resultArray = $this->buildReturnArray($result);
        return $resultArray;
    }

    /*
     * This request requires a user access token with the following scope:
        https://api.ebay.com/oauth/api_scope/sell.marketing.readonly
        https://api.ebay.com/oauth/api_scope/sell.marketing
     */
    function getItemPromotion($authToken, $promotionId){
        $this->clear();
        $this->set_method('GET');
        $this->set_data(false);
        $this->set_verb('item_promotion/'.urlencode($promotionId));
        $this->set_additionalUrlParameters('');
        $this->set_authToken($authToken);
        $result = $this->CallEbayMarketingAPI();
        $resultArray = $this->buildReturnArray($result);
        return $resultArray;
    }
    function updateItemPromotion($authToken, $promotionId){
        $this->clear();
        $this->set_method('PUT');
        $this->set_data(false);
        $this->set_verb('item_promotion/'.urlencode($promotionId));
        $this->set_additionalUrlParameters('');
        $this->set_authToken($authToken);
        $result = $this->CallEbayMarketingAPI();
        $resultArray = $this->buildReturnArray($result);
        return $resultArray;
    }
    //====================================================================
    //  PROMOTION CALLS
    //====================================================================
    /*
     * http://developer.ebay.com/Devzone/rest/api-ref/marketing/promotion__get.html
     */
    function getPromotions($authToken, $limit=null, $marketplaceId=null, $offset=null, $promotionStatus=null, $q=null, $sortField=null){
        $this->clear();
        $this->set_method('GET');
        $this->set_data(false);
        $this->set_verb('promotion');
        $this->set_additionalUrlParameters('limit='.urlencode($limit).'&marketplace_id='.urlencode($marketplaceId).'&offset='.urlencode($offset).'&promotion_status='.$promotionStatus.'&q='.urlencode($q).'&sort'.$sortField);
        $this->set_authToken($authToken);
        $result = $this->CallEbayMarketingAPI();
        $resultArray = $this->buildReturnArray($result);
        return $resultArray;
    }
    function pauseAPromotion($authToken, $promotionId){
        $this->clear();
        $this->set_method('POST');
        $this->set_data(false);
        $this->set_verb('promotion/'.urlencode($promotionId).'/pause');
        $this->set_additionalUrlParameters('');
        $this->set_authToken($authToken);
        $result = $this->CallEbayMarketingAPI();
        $resultArray = $this->buildReturnArray($result);
        return $resultArray;
    }
    function resumeAPromotion($authToken, $promotionId){
        $this->clear();
        $this->set_method('POST');
        $this->set_data(false);
        $this->set_verb('promotion/'.urlencode($promotionId).'/resume');
        $this->set_additionalUrlParameters('');
        $this->set_authToken($authToken);
        $result = $this->CallEbayMarketingAPI();
        $resultArray = $this->buildReturnArray($result);
        return $resultArray;
    }
    //====================================================================
    //  PROMOTION_SUMMARY_REPORT CALLS
    //====================================================================
    function getPromotionSummaryReport($authToken, $marketplaceId){
        $this->clear();
        $this->set_method('GET');
        $this->set_data(false);
        $this->set_verb('promotion_summary_report/');
        $this->set_additionalUrlParameters('marketplace_id='.urlencode($marketplaceId));
        $this->set_authToken($authToken);
        $result = $this->CallEbayMarketingAPI();
        $resultArray = $this->buildReturnArray($result);
        return $resultArray;
    }
    //====================================================================
    //  SUPPORTING FUNCTIONS
    //====================================================================

    function urlBuilder(){
        $apiCallType = $this->get_verb();
        $additionalUrlParameters = $this->get_additionalUrlParameters();

        $baseEndpoint = $this->accountApiEndpoint.$apiCallType;
        if($additionalUrlParameters != ''){
            $baseEndpoint.="?".$additionalUrlParameters;
        }
        return $baseEndpoint;
    }

    function CallEbayMarketingAPI(){
        $method = $this->get_method();
        $dataToSendToEbay = $this->get_data();
        $authToken = $this->get_authToken();

        $curl = curl_init();
        switch ($method)
        {
            case "POST":
                curl_setopt($curl, CURLOPT_POST, 1);

                if ($dataToSendToEbay)
                    curl_setopt($curl, CURLOPT_POSTFIELDS, $dataToSendToEbay);
                break;
            case "PUT":
                curl_setopt($curl, CURLOPT_PUT, 1);
                break;
            case "GET";
                curl_setopt($curl, CURLOPT_HTTPGET, 1);

                break;
            default:
                if ($dataToSendToEbay)
                    $url = sprintf("%s?%s", $url, http_build_query($dataToSendToEbay));
        }

        curl_setopt($curl, CURLOPT_HTTPHEADER, array(
            'Authorization:Bearer '. $authToken,
            'Accept:application/json',
            'Content-Type: application/json',
            'Content-Language: en-US'
        ));
        $url = $this->urlBuilder();
        curl_setopt($curl, CURLOPT_URL, $url);
        curl_setopt($curl, CURLOPT_RETURNTRANSFER, 1);

        $result = curl_exec($curl);

        curl_close($curl);

        return $result;
    }

    function clear(){
        $this->set_method('GET');
        $this->set_data(false);
        $this->set_verb('');
        $this->set_additionalUrlParameters('');
        $this->set_authToken('');
    }

    function buildReturnArray($result){
        $verb = $this->get_verb();
        $method = $this->get_method();
        $data = $this->get_data();
        $additionalUrlParameters = $this->get_additionalUrlParameters();

        return array(
            'verb' => $verb,
            'method' => $method,
            'data' => $data,
            'additionalUrlParameters' => $additionalUrlParameters,
            'result' => $result
        );
    }
}// END class ebayMarketingApi