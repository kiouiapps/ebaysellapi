<?php

class ebayFulfillmentApi{
    var $method;
    var $data;
    var $apiCallType;
    var $verb;
    var $additionalUrlParameters;
    var $authToken;

    public function __construct($fulfillmentApiEndpoint){
        $this->set_data(false);
        $this->accountApiEndpoint = $fulfillmentApiEndpoint;
    }

    function set_method($new_method){
        $this->method = $new_method;
    }

    function get_method(){
        return $this->method;
    }
    function set_data($new_data){
        $this->data = $new_data;
    }
    function get_data(){
        return $this->data;
    }
    function set_verb($new_verb){
        $this->verb = $new_verb;
    }
    function get_verb(){
        return $this->verb;
    }
    function set_additionalUrlParameters($new_additionalUrlParameters){
        $this->additionalUrlParameters = $new_additionalUrlParameters;
    }
    function get_additionalUrlParameters(){
        return $this->additionalUrlParameters;
    }

    function set_authToken($new_authToken){
        $this->authToken = $new_authToken;
    }
    function get_authToken(){
        return $this->authToken;
    }

/* These requests require a user access token with the following scope:
https://api.ebay.com/oauth/api_scope/sell.fulfillment
https://api.ebay.com/oauth/api_scope/sell.fulfillment.readonly
*/
    //====================================================================
    //  ORDER CALLS
    //====================================================================

    /*
     *  Currently, filter returns data from only the last 90 days.
     * If the orderIds parameter is included in the request,
     * the filter parameter will be ignored.
     */

    function getOrders($authToken, $filter=null, $limit=null, $offset=null, $orderIds=null){
        $this->clear();
        $this->set_method('GET');
        $this->set_data(false);
        $this->set_verb('order');
        $this->set_additionalUrlParameters('filter='.urlencode($filter).'&limit='.urlencode($limit).'&offset'.urlencode($offset).'&orderIds'.urlencode($orderIds));
        $this->set_authToken($authToken);
        $result = $this->CallEbayFulfillmentAPI();
        $resultArray = $this->buildReturnArray($result);
        return $resultArray;
    }
    function getAnOrder($authToken, $orderId){
        $this->clear();
        $this->set_method('GET');
        $this->set_data(false);
        $this->set_verb('order/'.urlencode($orderId));
        $this->set_additionalUrlParameters('');
        $this->set_authToken($authToken);
        $result = $this->CallEbayFulfillmentAPI();
        $resultArray = $this->buildReturnArray($result);
        return $resultArray;
    }

    //====================================================================
    //  SHIPPING_FULFILLMENT CALLS
    //====================================================================
    function createAShippingFulfillment($authToken, $orderId){
        $this->clear();
        $this->set_method('POST');
        $this->set_data(false);
        $this->set_verb('order/'.$orderId.'/shipping_fulfillment');
        $this->set_additionalUrlParameters('');
        $this->set_authToken($authToken);
        $result = $this->CallEbayFulfillmentAPI();
        $resultArray = $this->buildReturnArray($result);
        return $resultArray;
    }
    function getShippingFulfillments($authToken, $orderId){
        $this->clear();
        $this->set_method('GET');
        $this->set_data(false);
        $this->set_verb('order/'.$orderId.'/shipping_fulfillment');
        $this->set_additionalUrlParameters('');
        $this->set_authToken($authToken);
        $result = $this->CallEbayFulfillmentAPI();
        $resultArray = $this->buildReturnArray($result);
        return $resultArray;
    }
    function getAShippingFulfillment($authToken, $orderId, $fulfillmentId){
        $this->clear();
        $this->set_method('GET');
        $this->set_data(false);
        $this->set_verb('order/'.$orderId.'/shipping_fulfillment/'.$fulfillmentId);
        $this->set_additionalUrlParameters('');
        $this->set_authToken($authToken);
        $result = $this->CallEbayFulfillmentAPI();
        $resultArray = $this->buildReturnArray($result);
        return $resultArray;
    }

    //====================================================================
    //  SUPPORTING FUNCTIONS
    //====================================================================

    function urlBuilder(){
        $apiCallType = $this->get_verb();
        $additionalUrlParameters = $this->get_additionalUrlParameters();

        $baseEndpoint = $this->accountApiEndpoint.$apiCallType;
        if($additionalUrlParameters != ''){
            $baseEndpoint.="?".$additionalUrlParameters;
        }
        return $baseEndpoint;
    }

    function CallEbayFulfillmentAPI(){
        $method = $this->get_method();
        $dataToSendToEbay = $this->get_data();
        $authToken = $this->get_authToken();

        $curl = curl_init();
        switch ($method)
        {
            case "POST":
                curl_setopt($curl, CURLOPT_POST, 1);

                if ($dataToSendToEbay)
                    curl_setopt($curl, CURLOPT_POSTFIELDS, $dataToSendToEbay);
                break;
            case "PUT":
                curl_setopt($curl, CURLOPT_PUT, 1);
                break;
            case "GET";
                curl_setopt($curl, CURLOPT_HTTPGET, 1);

                break;
            default:
                if ($dataToSendToEbay)
                    $url = sprintf("%s?%s", $url, http_build_query($dataToSendToEbay));
        }

        curl_setopt($curl, CURLOPT_HTTPHEADER, array(
            'Authorization:Bearer '. $authToken,
            'Accept:application/json',
            'Content-Type: application/json',
            'Content-Language: en-US'
        ));
        $url = $this->urlBuilder();
        curl_setopt($curl, CURLOPT_URL, $url);
        curl_setopt($curl, CURLOPT_RETURNTRANSFER, 1);

        $result = curl_exec($curl);

        curl_close($curl);

        return $result;
    }

    function clear(){
        $this->set_method('GET');
        $this->set_data(false);
        $this->set_verb('');
        $this->set_additionalUrlParameters('');
        $this->set_authToken('');
    }

    function buildReturnArray($result){
        $verb = $this->get_verb();
        $method = $this->get_method();
        $data = $this->get_data();
        $additionalUrlParameters = $this->get_additionalUrlParameters();

        return array(
            'verb' => $verb,
            'method' => $method,
            'data' => $data,
            'additionalUrlParameters' => $additionalUrlParameters,
            'result' => $result
        );
    }

}