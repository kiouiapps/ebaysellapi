<?php

/**
 * Class Name: tokenApi
 * Author: Keisha Josephs
 * Author Email: kmjosephs@gmail.com
 * Description: V1
--------------------------------------------------------------------------
 *
        =Table of Contents
 *
 * 01. Construct Function
 * 02. Getters and Setters
 * 03. Credential Functions
 * 04. Curl Token Functions
 * 05. User Token Functions
 * 06. Application Token Function
 * 07. Redirect Url Function
 * 08. Supporting Functions
 * ------------------------------------------------------------------------
 */

class tokenApi {
    var $method;
    var $data;

    //====================================================================
    //              =01. CONSTRUCT FUNCTION
    //====================================================================

    public function __construct($clientId, $clientSecret, $RuName, $tokenApiEndpoint){
        $this->clientId = $clientId;
        $this->clientSecret = $clientSecret;
        $this->RuName = $RuName;
        $this->set_data(false);
        $this->tokenApiEndpoint = $tokenApiEndpoint;
    }
    //====================================================================
    //          =02. SETTERS AND GETTERS
    //====================================================================

    function set_method($new_method){
        $this->method = $new_method;
    } // puts it into the $method box

    function get_method(){
        return $this->method;
    } //takes it out of the $method box

    function set_data($new_data){
        $this->data = $new_data;
    }

    function get_data(){
        return $this->data;
    }

    //====================================================================
    //          =03. CREDENTIAL STRING FUNCTIONS
    //====================================================================

    /*
    * Ebay requires a base64 encoded credential string that consists of the clientId and the client Secret.
    * These can be obtained by going to your ebay developer account dashboard
    */

    function getCredentialString(){
        return $this->clientId.':'.$this->clientSecret;
    }

    function encodeBase64($parameter){
        return base64_encode($parameter);
    }

    function getCredentialStringEncoded(){
        return $this->encodeBase64($this->getCredentialString());
    }


    //====================================================================
    //          =04. CURL FUNCTION TO ACQUIRE TOKENS
    //====================================================================

    function curlTokenApi(){
        $method = $this->get_method();
        $data = $this->get_data();

        $curl = curl_init();
        switch ($method){
            case "POST":
                curl_setopt($curl, CURLOPT_POST, 1);

                if ($data)
                    curl_setopt($curl, CURLOPT_POSTFIELDS, $data);
                break;
            default:
                if ($data)
                    $this->tokenApiEndpoint = sprintf("%s?%s", $this->tokenApiEndpoint, http_build_query($data));
        }

        curl_setopt($curl, CURLOPT_HTTPHEADER, array(
            'Authorization:Basic '. $this->getCredentialStringEncoded(),
            'Content-Type: application/x-www-form-urlencoded',
        ));
        curl_setopt($curl, CURLOPT_URL, $this->tokenApiEndpoint);
        curl_setopt($curl, CURLOPT_RETURNTRANSFER, 1);
        $resultJson = curl_exec($curl);
        curl_close($curl);
        $resultArray = json_decode($resultJson, true);
        return $resultArray;
    }

    //====================================================================
    //          =05. USER TOKEN FUNCTIONS
    //====================================================================

    function getUserToken($code){
        $this->set_data("grant_type=authorization_code&code=$code&redirect_uri=".$this->RuName);
        $this->set_method('POST');
        $userTokenInfo = $this->curlTokenApi();
        return $userTokenInfo;
    }
    //When calling $scopeApiArray use ending like sell.account eg $newTokenObject -> getRefreshedToken(array('sell.account'))

    function getRefreshedToken($scopeApiArray, $refreshToken){
        $NewUserTokenUrl ="grant_type=refresh_token&";
        $NewUserTokenUrl .= "refresh_token=$refreshToken&";

        $scope = $this->scopeArrayToString($scopeApiArray);
        $NewUserTokenUrl .= "scope=$scope";

        $this->set_data($NewUserTokenUrl);
        $this->set_method('POST');

        $getNewUserTokenInfoArray = $this->curlTokenApi();
        $getNewUserTokenInfoArray['scopes']= $scopeApiArray;
        return $getNewUserTokenInfoArray;
    }

    //====================================================================
    //         =06. APPLICATION TOKEN FUNCTIONS
    //====================================================================

    function getApplicationToken(){
        $data = "grant_type=client_credentials&redirect_uri=".$this->RuName."&scope=https://api.ebay.com/oauth/api_scope";
        $this->set_data($data);
        $this->set_method('POST');
        $appTokenInfoArray = $this->curlTokenApi();
        return $appTokenInfoArray;
    }
    //====================================================================
    //          =07. REDIRECT URL FUNCTIONS
    //====================================================================
    /*
     * Returns an array consisting of the redirect Url and a state which is used as a parameter to this URL
     *
     * $scopeArray -- An array of APIs that you want to included at the end of the URL scopes required by eBay.
     * e.g. sell.account to make
     * $countryBrowserCode by default is set to com. You can pass a two-letter country code to override this
     * such as "de" for German or "it" for Italian. e.g. getRedirectUrl("de")
     * The following countries need "com" before the two letter codes
     * Australia - com.au, Hong Kong - com.hk, Malaysia - com.my, Singapore - com.sg, UK - co.uk
     *
     * Uses createStateToken from section =08.
    */
    function getRedirectUrl($scopeApiArray, $countryBrowserCode = "com"){
        $state = $this->createStateToken(20);

        $redirectUrl = "https://signin.ebay.".$countryBrowserCode."/authorize?";
        $redirectUrl .= "client_id=".$this->clientId."&";
        $redirectUrl .= "redirect_uri=".$this->RuName."&";
        $redirectUrl .= "response_type=code&state=$state&";
        $scope = $this->scopeArrayToString($scopeApiArray);
        $redirectUrl .= "scope=$scope";

        $redirectUrlArray = array(
            "redirectUrl" => $redirectUrl,
            "state" => $state,
            "scopes" => $scopeApiArray
        );

        return $redirectUrlArray;
    }



    //====================================================================
    //             =08. SUPPORTING FUNCTIONS
    //====================================================================

    //Creates a unique token for the state parameter in the redirect URL function (=07.) using crypto_rand_secure()
    function createStateToken($length)
    {
        $stateToken = "";
        $codeAlphabet = "ABCDEFGHIJKLMNOPQRSTUVWXYZ";
        $codeAlphabet.= "abcdefghijklmnopqrstuvwxyz";
        $codeAlphabet.= "0123456789";
        $max = strlen($codeAlphabet); // edited

        for ($i=0; $i < $length; $i++) {
            $stateToken .= $codeAlphabet[$this->crypto_rand_secure(0, $max-1)];
        }

        return $stateToken;
    }

    // Generates a random unique alphanumeric string
    function crypto_rand_secure($min, $max)
    {
        $range = $max - $min;
        if ($range < 1) return $min; // not so random...
        $log = ceil(log($range, 2));
        $bytes = (int) ($log / 8) + 1; // length in bytes
        $bits = (int) $log + 1; // length in bits
        $filter = (int) (1 << $bits) - 1; // set all lower bits to 1
        do {
            $rnd = hexdec(bin2hex(openssl_random_pseudo_bytes($bytes)));
            $rnd = $rnd & $filter; // discard irrelevant bits
        } while ($rnd > $range);
        return $min + $rnd;
    }

    function scopeArrayToString($scopeApiArray){
        $scopeUrl = " https://api.ebay.com/oauth/api_scope/";
        $scope = '';

        foreach($scopeApiArray as $scopeElement){
            $scope .= $scopeUrl.$scopeElement;
        }
        $scope = substr($scope, 1);
        $scope = urlencode($scope);
        return $scope;
    }

} //END class tokenApi()
