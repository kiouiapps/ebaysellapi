<?php

class ebayInventoryApi{
    var $method;
    var $data;
    var $apiCallType;
    var $verb;
    var $additionalUrlParameters;
    var $authToken;

    public function __construct($inventoryApiEndpoint){
        $this->set_data(false);
        $this->accountApiEndpoint = $inventoryApiEndpoint;

    }

    function set_method($new_method){
        $this->method = $new_method;
    }

    function get_method(){
        return $this->method;
    }
    function set_data($new_data){
        $this->data = $new_data;
    }
    function get_data(){
        return $this->data;
    }
    function set_verb($new_verb){
        $this->verb = $new_verb;
    }
    function get_verb(){
        return $this->verb;
    }
    function set_additionalUrlParameters($new_additionalUrlParameters){
        $this->additionalUrlParameters = $new_additionalUrlParameters;
    }
    function get_additionalUrlParameters(){
        return $this->additionalUrlParameters;
    }

    function set_authToken($new_authToken){
        $this->authToken = $new_authToken;
    }
    function get_authToken(){
        return $this->authToken;
    }

    //====================================================================
    //      INVENTORY_ITEM CALLS
    //====================================================================

    //This call will update the total quantity for one or more SKUs, and the price and/or quantity for one or more offers associated with those SKUs.
    // For calls, "requests" and "sku"  is required. To see a sample payload:
    function bulkUpdatePriceAndQuantity($authToken, $data){
        $this->clear();
        $this->set_method('POST');
        $this->set_data($data);
        $this->set_verb('bulk_update_price_quantity');
        $this->set_additionalUrlParameters('');
        $this->set_authToken($authToken);
        $result = $this->CallEbayInventoryAPI();
        $resultArray = $this->buildReturnArray($result);
        return $resultArray;
    }
    function createOrReplaceInventoryItem($authToken, $data, $sku){
        $this->clear();
        $this->set_method('PUT');
        $this->set_data($data);
        $this->set_verb('inventory_item/'.urlencode($sku));
        $this->set_additionalUrlParameters('');
        $this->set_authToken($authToken);
        $result = $this->CallEbayInventoryAPI();
        $resultArray = $this->buildReturnArray($result);
        return $resultArray;
    }

    function deleteInventoryItem($authToken, $sku){
        $this->clear();
        $this->set_method('DELETE');
        $this->set_data(false);
        $this->set_verb('inventory_item/'.urlencode($sku));
        $this->set_additionalUrlParameters('');
        $this->set_authToken($authToken);
        $result = $this->CallEbayInventoryAPI();
        $resultArray = $this->buildReturnArray($result);
        return $resultArray;
    }
    function getInventoryItem($authToken, $sku){
        $this->clear();
        $this->set_method('GET');
        $this->set_data(false);
        $this->set_verb('inventory_item/'.urlencode($sku));
        $this->set_additionalUrlParameters('');
        $this->set_authToken($authToken);
        $result = $this->CallEbayInventoryAPI();
        $resultArray = $this->buildReturnArray($result);
        return $resultArray;
    }
    function getInventoryItems($authToken, $limit, $offset){
        $this->clear();
        $this->set_method('GET');
        $this->set_data(false);
        $this->set_verb('inventory_item');
        $this->set_additionalUrlParameters('limit='.$limit.'&offset='.$offset);
        $this->set_authToken($authToken);
        $result = $this->CallEbayInventoryAPI();
        $resultArray = $this->buildReturnArray($result);
        return $resultArray;
    }
    //====================================================================
    //      INVENTORY_ITEM_GROUP CALLS
    //====================================================================
    function createOrReplaceInventoryItemGroup($authToken, $data, $inventoryItemGroupKey){
        $this->clear();
        $this->set_method('PUT');
        $this->set_data($data);
        $this->set_verb('inventory_item_group/'.urlencode($inventoryItemGroupKey));
        $this->set_additionalUrlParameters('');
        $this->set_authToken($authToken);
        $result = $this->CallEbayInventoryAPI();
        $resultArray = $this->buildReturnArray($result);
        return $resultArray;
    }
    function deleteInventoryItemGroup($authToken, $inventoryItemGroupKey){
        $this->clear();
        $this->set_method('DELETE');
        $this->set_data(false);
        $this->set_verb('inventory_item_group/'.urlencode($inventoryItemGroupKey));
        $this->set_additionalUrlParameters('');
        $this->set_authToken($authToken);
        $result = $this->CallEbayInventoryAPI();
        $resultArray = $this->buildReturnArray($result);
        return $resultArray;
    }
    function getInventoryItemGroup($authToken, $inventoryItemGroupKey){
        $this->clear();
        $this->set_method('GET');
        $this->set_data(false);
        $this->set_verb('inventory_item/'.urlencode($inventoryItemGroupKey));
        $this->set_additionalUrlParameters('');
        $this->set_authToken($authToken);
        $result = $this->CallEbayInventoryAPI();
        $resultArray = $this->buildReturnArray($result);
        return $resultArray;
    }
    //====================================================================
    //      LOCATION CALLS
    //====================================================================
    function createInventoryLocation($authToken, $data, $merchantLocationKey){
        $this->clear();
        $this->set_method('POST');
        $this->set_data($data);
        $this->set_verb('location/'.urlencode($merchantLocationKey));
        $this->set_additionalUrlParameters('');
        $this->set_authToken($authToken);
        $result = $this->CallEbayInventoryAPI();
        $resultArray = $this->buildReturnArray($result);
        return $resultArray;
    }
    function deleteInventoryLocation($authToken, $merchantLocationKey){
        $this->clear();
        $this->set_method('DELETE');
        $this->set_data(false);
        $this->set_verb('location/'.urlencode($merchantLocationKey));
        $this->set_additionalUrlParameters('');
        $this->set_authToken($authToken);
        $result = $this->CallEbayInventoryAPI();
        $resultArray = $this->buildReturnArray($result);
        return $resultArray;
    }
    function disableInventoryLocation($authToken, $data, $merchantLocationKey){
        $this->clear();
        $this->set_method('POST');
        $this->set_data($data);
        $this->set_verb('location/'.urlencode($merchantLocationKey).'/disable');
        $this->set_additionalUrlParameters('');
        $this->set_authToken($authToken);
        $result = $this->CallEbayInventoryAPI();
        $resultArray = $this->buildReturnArray($result);
        return $resultArray;
    }
    function enableInventoryLocation($authToken, $merchantLocationKey){
        $this->clear();
        $this->set_method('POST');
        $this->set_data(false);
        $this->set_verb('location/'.urlencode($merchantLocationKey).'/enable');
        $this->set_additionalUrlParameters('');
        $this->set_authToken($authToken);
        $result = $this->CallEbayInventoryAPI();
        $resultArray = $this->buildReturnArray($result);
        return $resultArray;
    }
    function getInventoryLocation($authToken, $merchantLocationKey){
        $this->clear();
        $this->set_method('GET');
        $this->set_data(false);
        $this->set_verb('location/'.urlencode($merchantLocationKey));
        $this->set_additionalUrlParameters('');
        $this->set_authToken($authToken);
        $result = $this->CallEbayInventoryAPI();
        $resultArray = $this->buildReturnArray($result);
        return $resultArray;
    }
    function getInventoryLocations($authToken, $maxLocationsToReturn, $zeroBasedStartingOffset ){
        $this->clear();
        $this->set_method('GET');
        $this->set_data(false);
        $this->set_verb('location');
        $this->set_additionalUrlParameters('limit='.$maxLocationsToReturn.'&offset='.$zeroBasedStartingOffset);
        $this->set_authToken($authToken);
        $result = $this->CallEbayInventoryAPI();
        $resultArray = $this->buildReturnArray($result);
        return $resultArray;
    }
    function UpdateInventoryLocation($authToken, $merchantLocationKey){
        $this->clear();
        $this->set_method('POST');
        $this->set_data(false);
        $this->set_verb('location/'.$merchantLocationKey.'/update_location_details');
        $this->set_additionalUrlParameters('');
        $this->set_authToken($authToken);
        $result = $this->CallEbayInventoryAPI();
        $resultArray = $this->buildReturnArray($result);
        return $resultArray;
    }
    //====================================================================
    //      OFFER CALLS
    //====================================================================
    function createOffer($authToken, $data){
        $this->clear();
        $this->set_method('POST');
        $this->set_data($data);
        $this->set_verb('offer');
        $this->set_additionalUrlParameters('');
        $this->set_authToken($authToken);
        $result = $this->CallEbayInventoryAPI();
        $resultArray = $this->buildReturnArray($result);
        return $resultArray;
    }
    function deleteOffer($authToken, $offerId){
        $this->clear();
        $this->set_method('DELETE');
        $this->set_data(false);
        $this->set_verb('offer/'.urlencode($offerId));
        $this->set_additionalUrlParameters('');
        $this->set_authToken($authToken);
        $result = $this->CallEbayInventoryAPI();
        $resultArray = $this->buildReturnArray($result);
        return $resultArray;
    }
    function getListingFees($authToken){
        $this->clear();
        $this->set_method('GET');
        $this->set_data(false);
        $this->set_verb('offer/get_listing_fees');
        $this->set_additionalUrlParameters('');
        $this->set_authToken($authToken);
        $result = $this->CallEbayInventoryAPI();
        $resultArray = $this->buildReturnArray($result);
        return $resultArray;
    }
    function getOffer($authToken, $offerId){
        $this->clear();
        $this->set_method('GET');
        $this->set_data(false);
        $this->set_verb('offer/'.urlencode($offerId));
        $this->set_additionalUrlParameters('');
        $this->set_authToken($authToken);
        $result = $this->CallEbayInventoryAPI();
        $resultArray = $this->buildReturnArray($result);
        return $resultArray;
    }
    function getOffers($authToken, $sku){
        $this->clear();
        $this->set_method('GET');
        $this->set_data(false);
        $this->set_verb('offer');
        $this->set_additionalUrlParameters('sku='.$sku);
        $this->set_authToken($authToken);
        $result = $this->CallEbayInventoryAPI();
        $resultArray = $this->buildReturnArray($result);
        return $resultArray;
    }
    function publishOffer($authToken, $data, $offerId){
        $this->clear();
        $this->set_method('POST');
        $this->set_data($data);
        $this->set_verb('offer/'.$offerId.'/publish');
        $this->set_additionalUrlParameters('');
        $this->set_authToken($authToken);
        $result = $this->CallEbayInventoryAPI();
        $resultArray = $this->buildReturnArray($result);
        return $resultArray;
    }
    function UpdateOffer($authToken, $data, $offerId){
        $this->clear();
        $this->set_method('PUT');
        $this->set_data($data);
        $this->set_verb('offer/'.$offerId);
        $this->set_additionalUrlParameters('');
        $this->set_authToken($authToken);
        $result = $this->CallEbayInventoryAPI();
        $resultArray = $this->buildReturnArray($result);
        return $resultArray;
    }
    //====================================================================
    //     PRODUCT_COMPATIBILITY CALLS
    //====================================================================
    function createOrReplaceProductCompatibility($authToken, $data, $sku){
        $this->clear();
        $this->set_method('PUT');
        $this->set_data($data);
        $this->set_verb('inventory_item/'.$sku.'/product_compatibility');
        $this->set_additionalUrlParameters('');
        $this->set_authToken($authToken);
        $result = $this->CallEbayInventoryAPI();
        $resultArray = $this->buildReturnArray($result);
        return $resultArray;
    }
    function deleteProductCompatibility($authToken, $sku){
        $this->clear();
        $this->set_method('DELETE');
        $this->set_data(false);
        $this->set_verb('inventory_item/'.$sku.'/product_compatibility');
        $this->set_additionalUrlParameters('');
        $this->set_authToken($authToken);
        $result = $this->CallEbayInventoryAPI();
        $resultArray = $this->buildReturnArray($result);
        return $resultArray;
    }
    function getProductCompatibility($authToken, $sku){
        $this->clear();
        $this->set_method('GET');
        $this->set_data(false);
        $this->set_verb('inventory_item/'.$sku.'/product_compatibility');
        $this->set_additionalUrlParameters('');
        $this->set_authToken($authToken);
        $result = $this->CallEbayInventoryAPI();
        $resultArray = $this->buildReturnArray($result);
        return $resultArray;
    }


    //====================================================================
    //  SUPPORTING FUNCTIONS
    //====================================================================

    function urlBuilder(){
        $apiCallType = $this->get_verb();
        $additionalUrlParameters = $this->get_additionalUrlParameters();

        $baseEndpoint = $this->accountApiEndpoint.$apiCallType;
        if($additionalUrlParameters != ''){
            $baseEndpoint.="?".$additionalUrlParameters;
        }
        return $baseEndpoint;
    }

    function CallEbayInventoryAPI(){
        $method = $this->get_method();
        $dataToSendToEbay = $this->get_data();
        $authToken = $this->get_authToken();

        $curl = curl_init();
        switch ($method)
        {
            case "POST":
                curl_setopt($curl, CURLOPT_POST, 1);

                if ($dataToSendToEbay)
                    curl_setopt($curl, CURLOPT_POSTFIELDS, $dataToSendToEbay);
                break;
            case "PUT":
                curl_setopt($curl, CURLOPT_PUT, 1);
                break;
            case "GET";
                curl_setopt($curl, CURLOPT_HTTPGET, 1);

                break;
            default:
                if ($dataToSendToEbay)
                    $url = sprintf("%s?%s", $url, http_build_query($dataToSendToEbay));
        }

        curl_setopt($curl, CURLOPT_HTTPHEADER, array(
            'Authorization:Bearer '. $authToken,
            'Accept:application/json',
            'Content-Type: application/json',
            'Content-Language: en-US'
        ));
        $url = $this->urlBuilder();
        curl_setopt($curl, CURLOPT_URL, $url);
        curl_setopt($curl, CURLOPT_RETURNTRANSFER, 1);

        $result = curl_exec($curl);

        curl_close($curl);

        return $result;
    }

    function clear(){
        $this->set_method('GET');
        $this->set_data(false);
        $this->set_verb('');
        $this->set_additionalUrlParameters('');
        $this->set_authToken('');
    }

    function buildReturnArray($result){
        $verb = $this->get_verb();
        $method = $this->get_method();
        $data = $this->get_data();
        $additionalUrlParameters = $this->get_additionalUrlParameters();

        return array(
            'verb' => $verb,
            'method' => $method,
            'data' => $data,
            'additionalUrlParameters' => $additionalUrlParameters,
            'result' => $result
        );
    }

} // END class ebayInventoryApi