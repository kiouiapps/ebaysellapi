<?php

class ebayAccountApi{
    var $method;
    var $data;
    var $apiCallType;
    var $verb;
    var $additionalUrlParameters;
    var $authToken;

    public function __construct($accountApiEndpoint){
        $this->set_data(false);
        $this->accountApiEndpoint = $accountApiEndpoint;

    }

    function set_method($new_method){
        $this->method = $new_method;
    }

    function get_method(){
        return $this->method;
    }
    function set_data($new_data){
        $this->data = $new_data;
    }
    function get_data(){
        return $this->data;
    }
    function set_verb($new_verb){
        $this->verb = $new_verb;
    }
    function get_verb(){
        return $this->verb;
    }
    function set_additionalUrlParameters($new_additionalUrlParameters){
        $this->additionalUrlParameters = $new_additionalUrlParameters;
    }
    function get_additionalUrlParameters(){
        return $this->additionalUrlParameters;
    }

    function set_authToken($new_authToken){
        $this->authToken = $new_authToken;
    }
    function get_authToken(){
        return $this->authToken;
    }
    //====================================================================
    //      FULFILLMENT_POLICY CALLS
    //====================================================================
    function createAFulfillmentPolicy($authToken, $data){
        $this->clear();
        $this->set_method('POST');
        $this->set_data($data);
        $this->set_verb('fulfillment_policy');
        $this->set_authToken($authToken);
        $this->set_additionalUrlParameters('');
        $result = $this->CallEbayAccountAPI();
        $resultArray = $this->buildReturnArray($result);
        return $resultArray;

    }
    function deleteAFulfillmentPolicy($authToken, $policyId){
        $this->clear();
        $this->set_method('DELETE');
        $this->set_data(false);
        $this->set_verb('fulfillment_policy/'.$policyId);
        $this->set_additionalUrlParameters('');
        $this->set_authToken($authToken);
        $result = $this->CallEbayAccountAPI();
        $resultArray = $this->buildReturnArray($result);
        return $resultArray;
    }
    function getFulfillmentPoliciesByMarketplace($authToken, $marketplaceId){
        $this->clear();
        $this->set_method('GET');
        $this->set_data(false);
        $this->set_verb('fulfillment_policy');
        $this->set_additionalUrlParameters("marketplace_id=".urlencode($marketplaceId));
        $this->set_authToken($authToken);
        $result = $this->CallEbayAccountAPI();
        $resultArray = $this->buildReturnArray($result);
        return $resultArray;
    }

    function getAFulfillmentPolicyById($authToken, $policyId){
        $this->clear();
        $this->set_method('GET');
        $this->set_data(false);
        $this->set_verb('fulfillment_policy/'.urlencode($policyId));
        $this->set_additionalUrlParameters('');
        $this->set_authToken($authToken);
        $result = $this->CallEbayAccountAPI();
        $resultArray = $this->buildReturnArray($result);
        return $resultArray;
    }
    function getAFulfillmentPolicyByName($authToken, $marketplaceId, $policyName){
        $this->clear();
        $this->set_method('GET');
        $this->set_data(false);
        $this->set_verb('fulfillment_policy/get_by_policy_name');
        $this->set_additionalUrlParameters("marketplace_id=".urlencode($marketplaceId)."&name=".urlencode($policyName));
        $this->set_authToken($authToken);
        $result = $this->CallEbayAccountAPI();
        $resultArray = $this->buildReturnArray($result);
        return $resultArray;
    }

    function updateAFulfillmentPolicy($authToken, $data, $policyId){
        $this->clear();
        $this->set_method('PUT');
        $this->set_data($data);
        $this->set_verb('fulfillment_policy/'.$policyId);
        $this->set_additionalUrlParameters('');
        $this->set_authToken($authToken);
        $result = $this->CallEbayAccountAPI();
        $resultArray = $this->buildReturnArray($result);
        return $resultArray;
    }

    //====================================================================
    //      PAYMENT_POLICY CALLS
    //====================================================================
    function deleteAPaymentPolicy($authToken, $policyId){
        $this->clear();
        $this->set_method('DELETE');
        $this->set_data(false);
        $this->set_verb('payment_policy/'.urlencode($policyId));
        $this->set_additionalUrlParameters('');
        $this->set_authToken($authToken);
    }
    function getPaymentPoliciesByMarketplace($authToken, $marketplaceId){
        $this->clear();
        $this->set_method('GET');
        $this->set_data(false);
        $this->set_verb('payment_policy');
        $this->set_additionalUrlParameters("marketplace_id=".urlencode($marketplaceId));
        $this->set_authToken($authToken);
        $result = $this->CallEbayAccountAPI();
        $resultArray = $this->buildReturnArray($result);
        return $resultArray;
    }
    function getAPaymentPolicyById($authToken, $policyId){
        $this->clear();
        $this->set_method('GET');
        $this->set_data(false);
        $this->set_verb('payment_policy/'.urlencode($policyId));
        $this->set_additionalUrlParameters('');
        $this->set_authToken($authToken);
        $result = $this->CallEbayAccountAPI();
        $resultArray = $this->buildReturnArray($result);
        return $resultArray;
    }
    function getAPaymentPolicyByName($authToken, $policyName, $marketplaceId){
        $this->clear();
        $this->set_method('GET');
        $this->set_data(false);
        $this->set_verb('payment_policy/get_by_policy_name');
        $this->set_additionalUrlParameters("name=".urlencode($policyName)."&marketplace_id=".urlencode($marketplaceId));
        $this->set_authToken($authToken);
        $result = $this->CallEbayAccountAPI();
        $resultArray = $this->buildReturnArray($result);
        return $resultArray;
    }
    //====================================================================
    //      PRIVILEGE CALLS
    //====================================================================
    function getAccountPrivileges($authToken){
        $this->clear();
        $this->set_method('GET');
        $this->set_data(false);
        $this->set_verb('privilege');
        $this->set_additionalUrlParameters('');
        $this->set_authToken($authToken);
        $result = $this->CallEbayAccountAPI();
        $resultArray = $this->buildReturnArray($result);
        return $resultArray;
    }
    //====================================================================
    //      PROGRAM CALLS
    //====================================================================
    function getOptedInPrograms($authToken){
        $this->clear();
        $this->set_method('GET');
        $this->set_data(false);
        $this->set_verb('program/get_opted_in_programs');
        $this->set_additionalUrlParameters('');
        $this->set_authToken($authToken);
        $result = $this->CallEbayAccountAPI();
        $resultArray = $this->buildReturnArray($result);
        return $resultArray;

    }
    //====================================================================
    //      RETURN_POLICY CALLS
    //====================================================================
    function deleteAReturnPolicy($authToken, $policyId){
        $this->clear();
        $this->set_method('DELETE');
        $this->set_data(false);
        $this->set_verb('return_policy/'.urlencode($policyId));
        $this->set_additionalUrlParameters('');
        $this->set_authToken($authToken);
    }
    function getReturnPoliciesByMarketplace($authToken, $marketplaceId){
        $this->clear();
        $this->set_method('GET');
        $this->set_data(false);
        $this->set_verb('return_policy');
        $this->set_additionalUrlParameters('marketplace_id='.urlencode($marketplaceId));
        $this->set_authToken($authToken);
        $result = $this->CallEbayAccountAPI();
        $resultArray = $this->buildReturnArray($result);
        return $resultArray;
    }
    function getAReturnPolicyById($authToken, $policyId){
        $this->clear();
        $this->set_method('GET');
        $this->set_data(false);
        $this->set_verb('return_policy/'.urlencode($policyId));
        $this->set_additionalUrlParameters('');
        $this->set_authToken($authToken);
        $result = $this->CallEbayAccountAPI();
        $resultArray = $this->buildReturnArray($result);
        return $resultArray;
    }
    function getAReturnPolicyByName($authToken, $policyName, $marketplaceId){
        $this->clear();
        $this->set_method('GET');
        $this->set_data(false);
        $this->set_verb('return_policy/get_by_policy_name');
        $this->set_additionalUrlParameters('name='.urlencode($policyName).'&marketplaceId='.urlencode($marketplaceId));
        $this->set_authToken($authToken);
        $result = $this->CallEbayAccountAPI();
        $resultArray = $this->buildReturnArray($result);
        return $resultArray;
    }

    //====================================================================
    //      SALES_TAX CALLS
    //====================================================================
    // Should $countryCode and $jurisdictionId be urlencoded?
    function deleteASalesTaxTable($authToken, $countryCode, $jurisdictionId){
        $this->clear();
        $this->set_method('DELETE');
        $this->set_data(false);
        $this->set_verb('sales_tax/'.urlencode($countryCode).'/'.urlencode($jurisdictionId));
        $this->set_additionalUrlParameters('');
        $this->set_authtoken($authToken);
    }
    function getASalesTaxTable($authToken, $countryCode, $jurisdictionId){
        $this->clear();
        $this->set_method('GET');
        $this->set_data(false);
        $this->set_verb('sales_tax/'.urlencode($countryCode).'/'.urlencode($jurisdictionId));
        $this->set_additionalUrlParameters('');
        $this->set_authtoken($authToken);
        $result = $this->CallEbayAccountAPI();
        $resultArray = $this->buildReturnArray($result);
        return $resultArray;
    }
    function getAllSalesTaxTable($authToken, $countryCode){
        $this->clear();
        $this->set_method('GET');
        $this->set_data(false);
        $this->set_verb('sales_tax');
        $this->set_additionalUrlParameters('country_code='.urlencode($countryCode));
        $this->set_authToken($authToken);
        $result = $this->CallEbayAccountAPI();
        $resultArray = $this->buildReturnArray($result);
        return $resultArray;
    }
    //====================================================================
    //  SUPPORTING FUNCTIONS
    //====================================================================

    function urlBuilder(){
        $apiCallType = $this->get_verb();
        $additionalUrlParameters = $this->get_additionalUrlParameters();

        $baseEndpoint = $this->accountApiEndpoint.$apiCallType;
        if($additionalUrlParameters != ''){
            $baseEndpoint.="?".$additionalUrlParameters;
        }
        return $baseEndpoint;
    }

    function CallEbayAccountAPI(){
        $method = $this->get_method();
        $dataToSendToEbay = $this->get_data();
        $authToken = $this->get_authToken();

        $curl = curl_init();
        switch ($method)
        {
            case "POST":
                curl_setopt($curl, CURLOPT_POST, 1);

                if ($dataToSendToEbay)
                    curl_setopt($curl, CURLOPT_POSTFIELDS, $dataToSendToEbay);
                break;
            case "PUT":
                curl_setopt($curl, CURLOPT_CUSTOMREQUEST, "PUT");
                if ($dataToSendToEbay)
                    curl_setopt($curl, CURLOPT_POSTFIELDS, $dataToSendToEbay);
                break;
            case "GET";
                curl_setopt($curl, CURLOPT_HTTPGET, 1);

                break;
            case "DELETE";
                curl_setopt($curl, CURLOPT_CUSTOMREQUEST, "DELETE");
                break;
            default:
                if ($dataToSendToEbay)
                    $url = sprintf("%s?%s", $url, http_build_query($dataToSendToEbay));
        }

        curl_setopt($curl, CURLOPT_HTTPHEADER, array(
            'Authorization:Bearer '. $authToken,
            'Accept:application/json',
            'Content-Type: application/json',
        ));
        $url = $this->urlBuilder();
        curl_setopt($curl, CURLOPT_URL, $url);
        curl_setopt($curl, CURLOPT_RETURNTRANSFER, 1);

        $result = curl_exec($curl);

        curl_close($curl);

        return $result;
    }

    function clear(){
        $this->set_method('GET');
        $this->set_data(false);
        $this->set_verb('');
        $this->set_additionalUrlParameters('');
        $this->set_authToken('');
    }

    function buildReturnArray($result){
        $verb = $this->get_verb();
        $method = $this->get_method();
        $data = $this->get_data();
        $additionalUrlParameters = $this->get_additionalUrlParameters();

        return array(
            'verb' => $verb,
            'method' => $method,
            'data' => $data,
            'additionalUrlParameters' => $additionalUrlParameters,
            'result' => $result
        );
    }
}// END class ebayAccountApi